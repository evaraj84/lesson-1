import React from 'react';
import Header from'./components/Header/Header';
import Footer from'./components/Footer/Footer';
import PageProduct from './components/PageProduct/PageProduct';


export default function PageProductMain() {
    return (
      <>
      <Header />
      <PageProduct />
      <Footer />
      </>
    );
  }