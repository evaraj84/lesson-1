import React from 'react';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import './App.css';
import PageProductMain from './PageProductMain';
import PageIndex from'./PageIndex';
import PageNotFound from'./PageNotFound';


export default function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<PageIndex />} />
          <Route path="/product" element={<PageProductMain />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

