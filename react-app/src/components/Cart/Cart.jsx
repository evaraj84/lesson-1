import { useDispatch, useSelector } from 'react-redux';
import { addProduct, removeProduct } from '../../reducers/cart-reducer';
import React, { useState } from 'react';
import './Cart.css';


export default function Cart(props) {
    const {product} = props;
    
    const [basketCount, setBasketCount] = useState (() => {
        const saved = localStorage.getItem('newGoodInBasket');
        return saved || '';
    }); 

    // useSelector - это хук, который импортируется из редакса, он отвечает за то чтобы получать какую то чать информации из хранилища
    // в функцию useSelector передаю все хранилище store, и возвращаю из store часть хранилища cart, из него - ключ products (который создала как массив), и  длину этого массива которая будет равна кол-ву товаров в корзине
    
    let products = useSelector((store) => store.cart.products);
    
    const dispatch = useDispatch(); // еще хук из библиотеки, он огтправляет данные в хранилище, отправить в хранилище ДЕЙСТВИЕ добавления в корзину, это действие импортирую из cart-reduser.js где оно создано 
    
    const alreadyInCart = products.some((prevProduct) => {return prevProduct.id === product.id});
    let count = products.length;  

    //функция проверки есть ли товар уже в корзине:    
        function handleAddProduct(e, product) {
                dispatch(addProduct(product));
                ++count;
                localStorage.setItem('newGoodInBasket', count);
                setBasketCount(count);
            }

        function handleRemoveProduct(e, product) {
                dispatch(removeProduct(product));
                localStorage.removeItem('newGoodInBasket');
                setBasketCount('');
        }
        return (

                <div className="basket-add">

                    {(alreadyInCart || basketCount) ? (
                        <button 
                        onClick={(e) => handleRemoveProduct(e, product)}
                        className="basket-add__btn btn-gray">Товар уже в корзине 
                        </button>
                    ) : (
                        <button 
                        onClick={(e) => handleAddProduct(e, product)}
                        className="basket-add__btn btn-orange">Добавить в корзину
                        </button>
                    )}
                   
                </div>

        )
    
}