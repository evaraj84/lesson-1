import React from 'react';
import './PageProduct.css';
import ColorCheck from'../ColorCheck/ColorCheck';
import MemoryConfig from '../MemoryConfig/MemoryConfig';
import Reviews from '../Reviews/Reviews';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import Form from '../Form/Form';
import Sidebar from '../Sidebar/Sidebar';
import Table from '../Table/Table';



function PageProduct() {
    return (

    <div className="container">
        <Breadcrumbs />
        <div className="block-photo">
            <h2 className="block-photo__h2">Смартфон Apple iPhone 13, синий</h2>
            <div className="iphone-blue-all-photo">
                <div className="iphone-blue"><img className="iphone-blue__img" src="/image-1.webp" alt="Айфон-3 внешний вид"></img></div>
                <div className="iphone-blue"><img className="iphone-blue__img" src="/image-2.webp" alt="Айфон-3 вид спереди"></img></div>
                <div className="iphone-blue"><img className="iphone-blue__img" src="/image-3.webp" alt="Айфон-3 вид сбоку"></img></div> 
                <div className="iphone-blue"><img className="iphone-blue__img" src="/image-4.webp" alt="Айфон-3 камера"></img></div> 
                <div className="iphone-blue"><img className="iphone-blue__img" src="/image-5.webp" alt="Айфон-3 вид сзади"></img></div> 
            </div> 
        </div>
        
        <main className="main-info">

            <div className="main-side"> 
                {/*      блок описания товара     */}
                <div className="discription">
                    <ColorCheck />
                    <MemoryConfig />

                    {/* <!-- список характеристик товара--> */}
                    <div className="caracteristic-ul">
                        <h3 className="heading3">Харатеристики товара</h3>
                        <ul className="caracteristic-ul__ul">
                            <li>Экран: <strong>6.1</strong></li>
                            <li>Операционная система: <strong>iOS 15</strong></li>
                            <li>Беспроводные интерфейсы: NFC, Bluetooth, Wi-Fi</li>
                            <li>Процессор: <strong><a className="a" href="https://ru.wikipedia.org/wiki/Apple_A15">Apple A15 Bionic</a></strong></li>
                            <li>Вес: <strong>173 г</strong></li>
                        </ul> 
                    </div>
                    {/* <!-- описание характеристик товара--> */}
                    <div className="caracteristic-text">
                        <h3 className="heading3">Описание</h3> 
                        <p className="caracteristic-text__p">Наша самая совершенная система двух камер.</p> 
                        <p>Особый взгляд на прочность дисплея.</p>
                        <p>Чип,с которым всё супербыстро.</p>
                        <p>Аккумулятор держится заметно дольше.</p>
                        <p><i>iPhone 13 - сильный мира всего.</i></p> 
                        <p className="caracteristic-text__p">Мы разработали совершенно новую схему расположения и развернулиобъективы на 45 градусов. Благодаря этому внутри корпуса поместилась нашалучшая система двух камер с увеличенной матрицей широкоугольной камеры.Кроме того, мы освободили место для системы оптической стабилизацииизображения сдвигом матрицы. И повысили скорость работы матрицы насверхширокоугольной камере.</p> 
                        <p className="caracteristic-text__p">Новая сверхширокоугольная камера видит больше деталей в тёмных областяхснимков. Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация сосдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
                        <p className="caracteristic-text__p">Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещенияфокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект размытиявокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другогочеловека или объект, который появился в кадре. Теперь ваши видео будут смотретьсякак настоящее кино.</p>
                    </div>
                </div>
                   
                <Table />

                {/* <!-- ОТЗЫВЫ--> */}
                <div className="all-reviews">
                    
                        <div  className="head-reviews">
                            <div className="head-reviews-h3">
                                <h3 className="head-reviews-h3__h3 head-reviews-h3__h3_no-margin">Отзывы</h3>
                            </div>
                            <div className="head-reviews-span">
                                <span className="head-reviews-span__span">425</span>
                            </div>
                        </div>
                  
                <Reviews />           
                </div> 
                <Form />
            </div> 

                <Sidebar />
        </main>

    </div>
    );
}
export default PageProduct;