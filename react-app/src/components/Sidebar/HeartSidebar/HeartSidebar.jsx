import './HeartSidebar.css';
import { useDispatch, useSelector } from 'react-redux';
import { addFavor, removeFavor } from '../../../reducers/favor-reducer';
import React, { useState } from 'react';

export default function HeartSidebar(props) {
    
    const {product} = props;
    
    const [FavorCount, setNewFavor] = useState (() => {
        const saved = localStorage.getItem('newFavor');
        return saved || '';
    }); 

    // useSelector - это хук, который импортируется из редакса, он отвечает за то чтобы получать какую то чать информации из хранилища
    // в функцию useSelector передаю все хранилище store, и возвращаю из store часть хранилища cart, из него - ключ products (который создала как массив), и  длину этого массива которая будет равна кол-ву товаров в корзине
    
    //let products = useSelector((store) => store.cart.products);
    let products = useSelector((store) => store.favorites.products);
    
    const dispatch = useDispatch();  
    
    const alreadyInFavor = products.some((prevProduct) => {return prevProduct.id === product.id});
    let countFavor = products.length;  

    function handleAddFavor(e, product) { // ф-ия вызывается по клику на сердечко
        dispatch(addFavor(product));
        countFavor++;
        localStorage.setItem('newFavor', countFavor);
        setNewFavor(countFavor);
    }

    function handleRemoveFavor(e, product) {
        dispatch(removeFavor(product));
        localStorage.removeItem('newFavor');
        setNewFavor('');
}   



// в зависимости от того есть ли уже товар в избранном - отображаю тот или другой блок с сердцем
// и вызываю функции убрать/добавить в избранное по событию клик

    return ( 
       <> 
    {(alreadyInFavor || FavorCount) ? (
        <div>
            <img onClick={(e) => handleRemoveFavor(e, product)} 
                className="heart-orange" 
                src = "heart-hover.svg" 
                alt="Добавлено в избранное"></img>
        </div>
    ) : (
        <div>
            <img  onClick={(e) => handleAddFavor(e, product)} 
                className="heart-gray" 
                src = './heart.svg' 
                alt="Не в избранном"
                onMouseOver={(e) => (e.currentTarget.src = './heart-hover.svg')}
                onMouseOut={(e) => (e.currentTarget.src = './heart.svg')}>
            </img> 
        </div>
    )}           
        </> 

    );

}
