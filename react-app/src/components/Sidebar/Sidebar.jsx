import React from 'react';
import './Sidebar.css';
import Cart from '../Cart/Cart';
import HeartSidebar from './HeartSidebar/HeartSidebar';


export default function Sidebar() {

    return ( 
        <aside className="sidebar">
                    
            <div className="price">
                <div className="old-price"> 
                    <div className="old-price-info">
                        <span className="old-price-info__span"><del>75 990₽</del></span>
                        <button className="old-price-info__btn">-8%</button>
                    </div>

            <HeartSidebar  product = {{id: 3, name: 'iPhone 13'}}/>
                
                </div> 
                <div className="new-price"> 
                    <span>67 990₽</span>
                </div> 
                <div className="delivery">
                    <p>Самовывоз в четверг, 1 сентября - <span className="delivery__span">бесплатно</span></p>
                    <p>Курьером в четверг, 1 сентября - <span className="delivery__span">бесплатно</span></p>
                </div>

            <Cart product = {{id: 1, name: 'iPhone 13'}}/>

            </div>

            <div className="advertise">
                    <span className="advertise__span">Реклама</span>
                    <div className="advertise-frame">
                        <iframe  title="frame1" className="advertise-frame__iframe" src="./advertise.html" width="420" height="300"></iframe>
                        <iframe  title="frame2" className="advertise-frame__iframe" src="./advertise.html" width="420" height="300"></iframe>
                    </div>
            </div>

    </aside>     


    );

}
