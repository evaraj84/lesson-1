import React from 'react';
import styles from "./Header.module.css";
import logo from "./favicon.svg";
import heart from "./heart.svg"; 
import basket from "./top-basket.png"; 
import { useSelector } from 'react-redux';

import { Link } from 'react-router-dom';


export default function Header() {
    
    useSelector((store) => store.cart.products);
    useSelector((store) => store.favorites.products);

    let isInBasket = localStorage.getItem('newGoodInBasket'); 
    let isInFavor = localStorage.getItem('newFavor'); 
    return ( 

        <header className={styles.header}>
            <div className={styles.inner}>

                <div className={styles.left}> 

                    <Link to='/'>
                    <img className={styles.logo} src={logo} alt="Логотип компании"></img></Link>
                    <Link to='/'><h1 className={styles.h1}><span className={styles.leftSpan}>Мой</span>Маркет</h1></Link>   

                </div>

                <div className={styles.right}>

                    <div className={styles.heart}>
                        <img className={styles.heartImg} src={heart} alt="Избранное"></img> 

                        {isInFavor && 
                        (<img className={styles.heartRound} src="https://static.tildacdn.com/tild6437-6661-4062-b634-316633336338/circle_PNG28.png" alt="Корзина"></img>)}
                        {isInFavor && <span className={styles.heartSpan}>{isInFavor}</span>}
                        <span className={styles.heartSpan}>{}</span>
                    </div>

                    <div className={styles.basket}>
                        <img className={styles.basketImg} src={basket} alt="Корзина"></img>
                    
                        {isInBasket && 
                        (<img className={styles.basketRound} src="https://static.tildacdn.com/tild6437-6661-4062-b634-316633336338/circle_PNG28.png" alt="Корзина">
                        </img>)}
                        {isInBasket && <span className={styles.basketSpan}>{isInBasket}</span>} 
                        <span className={styles.basketSpan}>{}</span> 

                    </div>

                </div>
            </div>  
        </header>
    );

}
