import React from 'react';
import { useCurrentDate } from "@kundinos/react-hooks";



export default function Year() {
    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();
    return(
        
            <span>{fullYear}</span>
        
        );
}


