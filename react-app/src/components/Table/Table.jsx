import React from 'react';
import './Table.css';

export default function Table() {
    return ( 
        
        <div className="table">
                        <h3 className="heading3">Сравнение моделей</h3>
                        <table className="table__caracteristic">
                            <tbody>
                            <tr className="table__row">
                                <th className="table__head">Модель</th>
                                <th className="table__head">Вес</th>
                                <th className="table__head">Высота</th>
                                <th className="table__head">Ширина</th>
                                <th className="table__head">Толщина</th>
                                <th className="table__head">Чип</th>
                                <th className="table__head">Объем памяти</th>
                                <th className="table__head">Аккумулятор</th>
                            </tr>
                            <tr className="table__row table__row_hovered">
                                <td className="table__data">iPhone 11</td>
                                <td className="table__data">194г</td>
                                <td className="table__data">150мм</td>
                                <td className="table__data">75мм</td>
                                <td className="table__data">8,3мм</td>
                                <td className="table__data">A13 Bio chip</td>
                                <td className="table__data">до 128гб</td>
                                <td className="table__data">до 17ч</td>    
                            </tr>
                            <tr className="table__row table__row_hovered">
                                <td className="table__data">iPhone 12</td>
                                <td className="table__data">164г</td>
                                <td className="table__data">146мм</td>
                                <td className="table__data">71мм</td>
                                <td className="table__data">7,4мм</td>
                                <td className="table__data">A14 Bio chip</td>
                                <td className="table__data">до 256гб</td>
                                <td className="table__data">до 19ч</td>
                            </tr>
                            <tr className="table__row table__row_hovered">
                                <td className="table__data">iPhone 13</td>
                                <td className="table__data">174г</td>
                                <td className="table__data">146мм</td>
                                <td className="table__data">71мм</td>
                                <td className="table__data">7,6мм</td>
                                <td className="table__data">A15 Bio chip</td>
                                <td className="table__data">до 512гб</td> 
                                <td className="table__data">до 19ч</td>
                             </tr>
                             </tbody>
                        </table>
                </div> 

    );
}
