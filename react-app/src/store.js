

import{ configureStore } from "@reduxjs/toolkit"; //импортирую функцию configureStore из библиотеки
import cartReducer from'./reducers/cart-reducer';
import favorReducer from'./reducers/favor-reducer';
let actionsCounter = 0;
const logger = (store) => (next) => (action) => { 
    
    ++actionsCounter;
    console.log("Количество действий: ", actionsCounter);
    console.log("action", action);
    
    let result = next(action);
    
    console.log("new state", store.getState());
    return result;
}

export const store = configureStore ({
    reducer: {
        cart: cartReducer,
        favorites: favorReducer
    },
    middleware: [logger],
   
});
