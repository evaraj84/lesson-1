import { Link } from 'react-router-dom';
import styled from"styled-components";
import Header from'./components/Header/Header';
import Footer from'./components/Footer/Footer';

const Container = styled.div`
    min-height: 100vh;
    display: flex;
    flex-direction: column;
`;

const Main = styled.div`
    flex: 1 0 auto;
`;

const Content = styled.div`
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    align-content: center;
    justify-content: center;
    overflow: auto;
`;

const P = styled.p`
    font-size: 16px; 
    text-align: center;
`;

const FooterStyle = styled.div`
flex-shrink: 0;
margin-top: auto;
`;

export default function PageIndex() {
    return (
        <>
        <Container>

            <Main>

                <Header></Header>

                <Content>
                    
                        <P> Здесь должно быть содержимое главной страницы.<br></br>
                            Но в рамках курса главная страница  используется лишь<br></br>
                            в демонстрационных целях </P>
                        <Link to='/product'>Перейти на страницу товара</Link>
                   
                </Content>
            
            </Main>
            <FooterStyle>
            <Footer></Footer>
            </FooterStyle>
        </Container>
            </>
    )
}