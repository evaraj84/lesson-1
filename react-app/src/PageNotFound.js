import React from 'react';
import styled from"styled-components";

const Header = styled.h1`
    font-size: 36px; 
    margin : 100px auto;
    text-align: center;
`;

const Image = styled.img`
    width: 250px; 
    height: auto;
    display: table;
    margin: 0 auto;
`;

export default function PageNotFound() {
    return (
    <>
        <Header>Page not found</Header>
        <Image src="https://krot.info/uploads/posts/2022-03/1646224969_40-krot-info-p-grustnii-kot-mem-smeshnie-foto-48.png" alt="sad cat"></Image>
    </>
    );
  }
  