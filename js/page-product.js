"use strict";

/* 
Необходимо написать скрипт для валидации формы “Добавить свой отзыв”.
Проверка поля ввода выполняется только при попытке отправить форму.
Если у какого-то поля появилась ошибка и пользователь хочет ввести что-то другое в это поле, 
то ошибка у данного поля скрывается, чтобы не мешать пользователю.
Если сразу несколько полей содержат ошибки, то должна быть показана только первая ошибка, чтобы избежать визуального шума.

Проверка формы на ошибки: 

1) Имя и Фамилия: 
- если поле не заполнено;
- введено меньше 2 символов;

2) Оценка (вывести сообщение "Оценка должна быть от 1 до 5" во всех трех случаях):
- поле не заполнено;
- в поле введены буквы вместо цифр;
- введены цифры не от 1 до 5;
*/

/* ДЗ-25
Реализовать сохранение неотправленной формы, чтобы пользователь мог 
вернуться вследующий раз и не начинать заполнение формы с нуля

если пользователь начал вводить какую-то информацию в любое из полей формы, то эта информация должна
быть подставлена в соответствующее поле ввода даже после перезагрузки страницы или браузера

если пользователь отправил форму и прошёл валидацию, то считаем, что они были успешно отправлены на сервер и
теперь хранить в браузере их не нужно — в таком случае после обновления страницы форма должна быть пустой
*/

// ПЕРЕМЕННЫЕ

//имя для всей формы:
let form = document.querySelector(".form");

// имя кнопки отправки формы заношу в переменную buttonForm: 
let buttonForm = document.querySelector(".form-inner__btn");

// имя поля "введите имя" - в переменную:
let inputName = document.querySelector(".name-rate__name"); 

// имя поля "оценка" в переменную:
let inputRate = document.querySelector(".name-rate__rate");

// создаю 2 переменные, которые будут содержать тексты ошибок в оранжевых боксах
let nameErrorText = document.querySelector(".name-error"); // ошибка  имени
let rateErrorText = document.querySelector(".rate-error"); // ошибка оценки

// создаю массив с ошибками:
let errors = [
    "Вы ввели слишком короткое имя!",
    "Вы не заполнили имя!",
    "Оценка должна быть от 1 до 5"
];

// задаю переменные, в которых будут храниться значения введенное в полях input "введите имя" и "оценка"
// значения этих двух переменных также будут сохраняться в Local storage если форма не отправлена
let nameValue = ""; 
let rateValue = ""; 

inputName.value=localStorage.getItem('userNameStorage');
inputRate.value=localStorage.getItem('userRateStorage');

// СОБЫТИЯ

// обрабатываю событие отправки: отменяю свойство формы по умолчанию - ее отправку после нажатия submit
function handleSubmit(event) {
    event.preventDefault();
}

// задаю имя для функции handleSubmit
form.addEventListener("submit", handleSubmit);

// подписываюсь на событие итпут поля Имя Фамилия и заношу введенное имя в переменную:
inputName.addEventListener("input", (event) => {
    nameValue = event.target.value;
    // здесь же, как только юзер ввел символы, устанавливаю Local (не Session) Storage, т.к. мне надо, чтобы данные в форме сохранялись даже если юзер закрыл браузер, который будет хранить значение введенного Имени 
    localStorage.setItem('userNameStorage', nameValue);  //userNameStorage - будет именем ключа, nameValue - значение
 });
 
 // подписываюсь на событие итпут поля Оценка и заношу оценку в переменную:
 inputRate.addEventListener("input", (event) => {
    rateValue = event.target.value;
    localStorage.setItem('userRateStorage', rateValue); //аналогично для  оценки
 });
 
 // подписываюсь на событие клик по кнопке формы:
buttonForm.addEventListener("click", function() {
     
    if (inputName.value.length === 1) { // вывожу ошибку если имя = 1 (то есть меньше 2 символов, но не 0)
        nameErrorText.style.visibility="visible"; //делаю видимым оранжевый блок
        nameErrorText.innerHTML = errors[0]; // ввожу текст в div с классом .name-error, т.е. для элемента nameErrorText
    } 
    else if (inputName.value == "") { // ошибку если  пустое поле с именем
        nameErrorText.style.visibility="visible";
        nameErrorText.innerHTML = errors[1]; 
    } 
    else if (inputRate.value == "" ||
            (inputRate.value < 1 || inputRate.value > 5) ||
            isNaN(inputRate.value)) { // ошибки если пусто поле с оценкой или значение не равно от 1 до 5 или введено не число
        rateErrorText.style.visibility="visible";
        rateErrorText.innerHTML = errors[2]; 
    } 
    else {
        console.log("Введенное имя: " + nameValue + ", оценка: " +  rateValue);
        // если ошибок не было, считаем, что форма отправлена и тогда очищаем Storage
        
        localStorage.removeItem('userNameStorage');
        localStorage.removeItem('userRateStorage');
        // и очищаю поля инпут после нажатия по кнопке:
        inputName.value="";
        inputRate.value="";
    }
 
 });
 
 // убираю оранжевый бокс если пользователь хочет ввести имя заново
 inputName.addEventListener("click", function() { //если юзер кликает по полю инпут, то
     nameErrorText.style.visibility="hidden"; 
 });
 
 // убираю оранжевый бокс если пользователь хочет ввести число заново
 inputRate.addEventListener("click", function() {
     rateErrorText.style.visibility="hidden"; 
 });

 
// АТТЕСТАЦИЯ   АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ  АТТЕСТАЦИЯ

// добавление в ИЗБРАННОЕ 

// нахожу кнопку "добавить в избранное" и присваиваю ее переменной:
let buttonHeart = document.querySelector(".old-price-heart");

// нахожу блок который хочу отобразить при клике мышкой на сердечко (оно станет полностью оранжевым):
let buttonHeartAdded = document.querySelector(".old-price-heart-added");

// нахожу красный кружок, который должен появляться над сердцем если юзер добавил в избранное
let redRoundHeart = document.querySelector(".heart__img-round");

// нахожу теги span между которыми будет появляться кол-во избранных товаров (пока у нас 1 товар)
let goodAmountHeart = document.querySelector(".heart__span");

// и по умолчанию отображаю в этой переменной значение которое будет сохраняться в local storage, чтобы кол-во избранных товаров отображалось при перезагрузке
goodAmountHeart.innerHTML = localStorage.getItem('heartOne'); // heartOne задаю ниже

// создаю функцию, которая будет отображать элементы на странице если товар добавлен в избранное
function addRedHeart() { 
    buttonHeartAdded.style.display = "inline-block"; // показать оранжевое сердце
    buttonHeart.style.visibility = "hidden"; // спрятать блок с серым сердцем
    redRoundHeart.style.display = "block"; // показать красный кружок над сердцем в  шапке
}

// обрабатываю событие клик по кнопке "серое сердце"
buttonHeart.addEventListener("click", function() { 
   
    goodAmountHeart.innerHTML++; // если клик - то отображен на один товар больше в избранное в кружке 
    localStorage.setItem("heartOne", goodAmountHeart.innerHTML); // кол-во товаров в избранном сохраняю в local storage, чтобы значение сохранилось если юзер покинул страницу 
    addRedHeart(); // вызываю функцию, чтоб отобразить активные элементы на странице

});

if (localStorage.getItem('heartOne')) { // при перезагрузке страницы - если в памяти хранится значение в локале, то отображаю активные элементы:
    addRedHeart();
} else {}


buttonHeartAdded.addEventListener("click", function() { //такое же событие по клику на оранжевое сердце, которое стало видимым после события "клик"
        
    buttonHeart.style.visibility = "visible"; // наоборот, серое делаю видимым
    buttonHeartAdded.style.display = "none"; // и оранжевое прячу
    redRoundHeart.style.display = "none"; // убираю красный круг над сердцем
    goodAmountHeart.innerHTML = ""; // убираю контент
    localStorage.removeItem('heartOne'); //удаляю количество избранных товаров из памяти браузера

});


// ДОБАВЛЕНИЕ В КОРЗИНУ (по аналогии все так же):

// нахожу контейнер из html в котором будет отображаться кол-во добавленных товаров в шапке сайта над корзинкой:
let goodsAmountBasket = document.querySelector(".basket__span");

// нахожу красный кружок который должен появиться над корзиной в случае добавления товара
let redRoundBasket = document.querySelector(".basket__img-round");

// нахожу кнопку "добавить в корзину"
let btnAddToBasket = document.querySelector(".basket-add__btn");

// в localStorage сохраняю значение = кол-ву добавленных в корзину товаров
goodsAmountBasket.innerHTML = localStorage.getItem('basketOne'); 

// обработка события клик по кнопке "добавить в корзину"
btnAddToBasket.addEventListener("click", function() { 

    if (!localStorage.getItem('basketOne')) { // если еще в local storage не было информации о товаре в корзине то отображаю и меняю элементы:
        goodsAmountBasket.innerHTML = "1"; // если клик - то отображен один товар в корзине 
        localStorage.setItem("basketOne", goodsAmountBasket.innerHTML); // кол-во товаров в корзине сохраняю в local storage
        redRoundBasket.style.display = "block"; // одновременно появляется красный кружок над корзинкой
        btnAddToBasket.innerHTML = "Товар добавлен в корзину";
    }

    else { // иначе если данные уже хрянятся в local storage, то повторный клик по кнопке удалит их
        localStorage.removeItem('basketOne');
        redRoundBasket.style.display = "none";
        goodsAmountBasket.innerHTML = ""; 
        btnAddToBasket.innerHTML = "Добавить в корзину";
    }

});

if (localStorage.getItem('basketOne')) { // при перезагрузке страницы отображаю элементы:
    
    redRoundBasket.style.display = "block"; 
    btnAddToBasket.innerHTML = "Товар добавлен в корзину";
    
 } else {}



