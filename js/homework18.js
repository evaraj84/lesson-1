"use strict";

//Упражнение 1 Вывести в консоль 423
let a='100px';
let b='323px';
let result=parseInt(a)+parseInt(b);
console.log(result); 

//Упражнение 2 Вывести в консоль максимальное число из: 10, -45, 102, 36, 12, 0, -1
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Упражнение 3 - написать решение для каждого случая:

let d=0.111; // Округлить до 1
console.log(Math.ceil(d));

let e=45.333333; // Округлить до 45.3
console.log(e.toFixed(1));

let f=3; // Возвести в степень 5 (должно быть 243)
console.log(f**5);

let g=400000000000000; // Записать в сокращенном виде
console.log(4e14);

let h = '1'!=1; // Поправить условие, чтобы результатбыл true (значения изменять нельзя, только оператор)
let u = '1'!==1; //проверяю строгим неравенством одинаковы ли типы данных
console.log(u);

//Упражнение 4 - Вернёт false, почему?
console.log(0.1+0.2===0.3); // false
//нашла ответ в статье по ссылки Юрия: 0.1 , 0.2 , 0.3 - бесконечные дроби в бинарной системе. В JavaScript нет возможности для хранения точных значений 0.1 или 0.2, используя двоичную систему, точно также, как нет возможности хранить одну третью в десятичной системе счисления (0,33333...). Числовой формат IEEE-754 решает эту проблему путём округления до ближайшего возможного числа. 

//Если вывести что такое на самом деле 0.2 в вдоичной системе:

//alert( 0.2.toFixed(20) ); // вернет 0.20000000000000001110 поэтому 0,1+0,2 - это не точно 0,3
//поэтому можно округлить 0,1 и 0,2 чтобы получить true

//Чтобы получить true помогает округление значений до целого числа (надеюсь так делать правильно но я не уверена) :
let w = Math.round(0.1)+Math.round(0.2)===Math.round(0.3);
console.log(w); // вернет  true

