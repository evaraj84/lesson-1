// Упражнение 1 

/* Напишите функциюgetSumm(arr), которая принимает любоймассив и возвращает сумму чисел в этом массиве 
   В качестве элементов массива могут приходитьне только числа и такие элементы необходимо пропускать */

 /** Функция суммирует все числовые элементы массива, не числовые - пропусакает
  * @type {any[]} arr - массив с элементами разных типов 
  * @return {number[]} arrFilter - сначала создаю массив, отфильтрованный с помощью функции filter(),
  *  где элементы - только числа (проверяю является ли элемент массива числом с помощью метода Number.isFinite)
  * @param {number} sum - сумма всех числовых элементов из массива arrFilter
 */
let arr = [1, 2, 4, 1, "a", {}, 3];

function getSumm(arr) {
    let arrFilter = arr.filter(function (num) {
        return Number.isFinite(num);
    });
    
    let sum = 0;
    
    for (let num in arrFilter) {
        sum = sum + Number(arrFilter[num]);
    }
    return sum;
}
console.log( getSumm(arr) );

// Упражнение 2  
// в файле data.js 


// Упражнение 3  

/*Реализуйте упрощенную корзину в интернет-магазине.Представим, что мы храним в корзине идентификаторы товаров, 
которые пользователь добавил в корзину и нам нужно реализовать две функции: добавить в корзину и удалить из корзины. 
Причём в корзину нельзя добавить два одинаковых товара */

/** Функция addToCart будет добавлять товар в корзину, removeFromCart - удалять
 * @param {any[]} cart - массив с идентификаторами товаров в корзине
 * @param {number} index - индекс элемента (искомого товара) в массиве, который нужно удалить
 * @return {any[]} cart - возвращает измененный массив 
 */

let cart = [4884, 5555, 1111];

function addToCart(num) {

    if (cart.includes(num)===false){ //если проверка на присутствие num - ложь, т.е. в массиве не существует num
    cart.push(num); //тогда добавляю num
    }

    return cart; 

}

console.log(addToCart(5555));

function removeFromCart(num) {

    let index = cart.indexOf(num); 

    if (index >= 0) { //если индекс больше 0, т.е искомый элемент в массиве существует
        cart.splice(index, 1); //то с помощью метода splice вырезаю элемент с индексом index
       
    }

    return cart;
    
}

console.log(removeFromCart(4884));