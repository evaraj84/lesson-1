"use strict";

// Упражнение 2
/* Отредактируйте данный файл и добавьте в объект товара:
● массив ссылок на все изображения товара (берем изhtml)
● массив строк всех возможных цветов у данного товара
● массив строк всех возможных конфигураций памяти
● массив возможных вариантов доставки, где каждыйвариант доставки описан в виде объекта
*/
let product = {
    name: "Apple iPhone 13",
    color: "blue",
    memory: 256,
    characteristics: {
        screen: 6.1,
        oS: "iOS 15",
        wireless: ['NFC', 'Bluetooth', 'Wi-Fi'],
        processor: "Apple A15 Bionic",
        weight: 173
    },
    prices: {
        price: 67990,
        oldPrice: 75990,
        discount: 8
    },
    images: ['images/color-1.webp',
             'images/color-2.webp',
             'images/color-3.webp',
             'images/color-4.webp',
             'images/color-5.webp',
             'images/color-6.webp'
            ],
    colors: ['red', 'black', 'pink', 'blue', 'silver', 'vinous'],
    memories: [128, 256, 512],
    delivery: [
        {name: 'Самовывоз', date: 'четверг, 1 сентября', cost: 0},
        {name: 'Курьером', date: 'четверг, 1 сентября', cost: 0}
    ]
}

let review1 = {
    userName: "Марк.Г",
    rate: 5,
    userPhoto: "../images/review-1.jpeg",
    usageExperience: `менее месяца`,
    advantages: `это мой первый айфон после после огромного количества телефонов на андроиде. 
    всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.`,
    disadvantages: `к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) 
    а если нужно переносить фото с компа, то это только через itunes, которыйурезает 
    качество фотографий исходное`
}

let review2 = {
    userName: "Валерий Коваленко",
    rate: 4,
    userPhoto: "../images/review-2.jpeg",
    usageExperience: `менее месяца`,
    advantages: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго.`,
    disadvantages: `Плохая ремонтопригодность`
}

