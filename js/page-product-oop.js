"use strict"

let form = document.querySelector('.form'); // нахожу форму

let inputName = document.querySelector('.name-rate__name'); // нахожу Инпут Имя
let inputGrade = document.querySelector('.name-rate__rate'); // инпут для введения оценки заношу в переменную
let inputText = document.querySelector('.form-inner__textarea-review') // заношу в переменную textarea формы

let mistakeGrade = document.querySelector('.rate-error'); // блок для вывода ошибки оценки присваиваю переменной
let mistakeName = document.querySelector('.name-error'); // нахожу блок с ошибкой введенного имени

//по умолчанию заношу в поля инпута значения, сохраненные в local storage
inputName.value = localStorage.getItem('name'); 
inputGrade.value = localStorage.getItem('grade');
inputText.value = localStorage.getItem('review');

class AddReviewForm {
    // передаю в конструктор переменные, в которые присваила значения из html
    constructor(form, inputName, mistakeName, inputGrade, mistakeGrade, inputText) {
        this.form = form;
        this.inputName = inputName;
        this.mistakeName = mistakeName;
        this.inputGrade = inputGrade;
        this.mistakeGrade = mistakeGrade;
        this.inputText = inputText;
    }

    validate(event) { // описываю метод класса - валидацию формы
        event.preventDefault()  
        // для дальнейшей валидации введенного пользователем имени убираю пробелы если они есть и определяю длину:
        let nameLength = inputName.value.trim().length; 
        //значение введенного имени без пробелов:
        let nameSymbols = inputName.value.trim();
        //значение введенной оценки, приведенное к числу, без пробелов:
        let grade = + inputGrade.value.trim();
    
        if (nameLength >= 3) {
            console.log('ok');
    
        } else if (nameSymbols === '') {  // пустое поле ввода
            mistakeName.style.visibility="visible";
            mistakeName.innerHTML = 'Вы не ввели имя!'; 
            return;
    
        } else {// вывожу ошибку если имя короткое
                mistakeName.style.visibility="visible"; //делаю видимым оранжевый блок
                mistakeName.innerHTML = 'Слишком короткое имя!'; // ввожу текст в div с классом .name-error, т.е. для элемента nameErrorText
            return;
        }
    
        if (grade > 0 && grade < 6) {  // 
            console.log('OK');
    
        } else {
            mistakeGrade.style.visibility="visible";
            mistakeGrade.innerHTML = 'Оценка должна быть от 1 до 5';
            return;
        }
    }

    clearmistakeName() { // очищаю поле инпут с именем если пользователь хочет ввести заново имя:
        mistakeName.style.visibility="hidden"; // прячу оранжевый блок
        mistakeName.innerHTML = ''; //убираю надпись с предупреждением
    }
    
    
    clearmistakeGrade() { // так же по аналогии очищаю поле инпут с оценкой если пользователь хочет ввести заново оценку:
        mistakeGrade.style.visibility="hidden"; 
        mistakeGrade.innerHTML = '';  
    }
    
    changeName() { //сохраняю в памяти браузера все введенные юзером данные:
        localStorage.setItem('name', inputName.value);
    }
    
    changeGrade() {
        localStorage.setItem('grade', inputGrade.value);
    }
    changetext() {
        localStorage.setItem('review', inputText.value);
    }
}


let add_listener_form = new AddReviewForm(form,inputName,mistakeName,inputGrade,mistakeGrade,inputText);

form.addEventListener('submit', add_listener_form.validate); // вызываю метод validate при событии submit формы
inputName.addEventListener('focus', add_listener_form.clearmistakeName); // вызываю метод очистки формы имени при событии фокуса инпут нэйм
inputGrade.addEventListener('focus', add_listener_form.clearmistakeGrade); // тож самое для поля оценки
inputName.addEventListener('input', add_listener_form.changeName); //вызываю метод changename при новом вводе имени,которое будет сохраняться в локал сторэдж
inputGrade.addEventListener('input', add_listener_form.changeGrade);
inputText.addEventListener('input', add_listener_form.changetext);


